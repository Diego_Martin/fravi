'use strict'

function multiple (bases = []) {
	class Bases {
		constructor () {
			bases.forEach (BaseClass => Object.assign (this, new BaseClass ()))
		}
	}

	bases.forEach (BaseClass => {
		Object.getOwnPropertyNames (BaseClass.prototype)
		.filter (prop => prop != 'constructor')
		.forEach (prop => Bases.prototype [prop] = BaseClass.prototype [prop])
	})

	return Bases
}

class Runnable {
	post_constructor_set_loading_bar () {

		if (this._loading_bar != undefined) {
			let that = this
			this._request?.addEventListener?.('readystatechange', () => {
				switch (this._request.readyState) {
					case 1:
						that._loading_bar.getElementsByTagName ('div')[0].style.width = '25%'
						break
	
					case 2:
						that._loading_bar.getElementsByTagName ('div')[0].style.width = '50%'
						break
	
					case 3:
						that._loading_bar.getElementsByTagName ('div')[0].style.width = '75%'
						break
	
					default:
						break
				}
			})
		}
	}

	run (...args) {
		console.error ('Not implemented')
		throw new Error ('Not implemented')
	}
}

class Initiable {
	init (...args) {
		console.error ('Not implemented')
		throw new Error ('Not implemented')
	}
}

class FraviApp extends Initiable {
	constructor (loading_bar = null, onpushstate = null, controllers = [], animations = []) {
		super ()
		this._loading_bar = loading_bar
		this._controllers = [...controllers]
		this._animations = [...animations]
		// this._request = new XMLHttpRequest ()
		this._visited = []
		this._onpushstate = (onpushstate == null) ? () => {} : onpushstate

		let that = this
		window.addEventListener ('popstate', event => {
			that.change_state_event_handler (event)
		})
		
		window.addEventListener ('pushstate', event => {
			that.change_state_event_handler (event)
		})

		// if (this._loading_bar != null) {
		// 	this._request.addEventListener ('readystatechange', () => {
		// 		switch (this._request.readyState) {
		// 			case 1:
		// 				that._loading_bar.getElementsByTagName ('div')[0].style.width = '25%'
		// 				break
	
		// 			case 2:
		// 				that._loading_bar.getElementsByTagName ('div')[0].style.width = '50%'
		// 				break
	
		// 			case 3:
		// 				that._loading_bar.getElementsByTagName ('div')[0].style.width = '75%'
		// 				break
	
		// 			default:
		// 				break
		// 		}
		// 	})
		// }
		Object.seal (this)
		Object.freeze (this)
	}

	init (before_request = null) {
		let that = this
		this._controllers.forEach (controller => {
			try {
				if (!controller.initialised) {
					controller.init (this, () => {
						that.link_event_listener ('[data-link]', 'data-link')
						// that.link_a_event_listener ('a', 'href')
						if (before_request != null)
							before_request ()
					})
					console.log (controller.route + ' inicializado')
					if (controller.loading_bar == null)
						controller.loading_bar = that._loading_bar

					controller.animations = this._animations
				}
			} catch (error) {
				console.log (error)
				console.log ('Controller not initialised')
			}
		})

		this.change_state_event_handler ()
	}

	change_state_event_handler (event = {}) {
		if (this._loading_bar) {
			this._loading_bar.getElementsByTagName ('div')[0].style.width = '0%'
			if (this._loading_bar.classList.contains ('d-none'))
				this._loading_bar.classList.remove ('d-none')
		}
		this._controllers.forEach (controller => {
			if (controller.route.split ('?') [0] == window.location.pathname) {
				console.log (controller.route)
				// console.log (window.location.pathname)
				// console.log (window.location.href)
				try {
					if (window.location.href.split ('?').length == 1) {
						// controller.run (this._request)
						controller.run ()
					} else {
						let parts = window.location.href.split ('?')
						parts.shift ()
						
						// controller.run (this._request, parts.join ('?'))
						controller.run (parts.join ('?'))
					}
				} catch (error) {
					console.log ('Controller cannot run')
				}
			}
		})
	}

	add_controller (controller) {
		this._controllers.push (controller)
	}

	execute_controller (path, event = {}) {
		let that = this
		this._controllers.forEach (controller => {
			if (controller.route.split ('?') [0] == path) {
				window.history.pushState (null, null, controller.route)
				that.change_state_event_handler (event)
				that._onpushstate ()
			}
		})
	}

	link_event_listener (selector, attribute) {
		let that = this
		// console.log (document.querySelectorAll (selector))
		document.querySelectorAll (selector).forEach (element => {
			if (!element.hasAttribute ('data-TlFwszPG')) {
				element.addEventListener ('mousedown', event => {
					// console.log (`Enlace ${element} clicado`)
					// console.log (event.button)
					// console.log (event.type)
					if (element.tagName.toLowerCase () != 'a') {
						// Cargo la página a la que me dirijo
						if (event.button == 1) { // Click en la rueda
							window.open (element.getAttribute (attribute), '_blank')
						} else if (event.button == 0) { // Click izquierdo
							if (element.hasAttribute ('data-page-title')) {
								window.history.pushState (null, element.getAttribute ('data-page-title'), element.getAttribute (attribute))
								document.title = element.getAttribute ('data-page-title')
							} else {
								window.history.pushState (null, null, element.getAttribute (attribute))
							}
							that.change_state_event_handler (event)
							that._onpushstate ()
						}
					}
				})
				element.setAttribute ('data-TlFwszPG', 'CgfrORUh')
			}
		})
	}

	link_a_event_listener (selector, attribute) {
		let that = this
		document.querySelectorAll (selector).forEach (element => {
			if (!element.hasAttribute ('data-TlFwszPG')) {
				element.addEventListener ('click', event => {
					console.log (`Enlace ${element} clicado`)
					// Cargo la página a la que me dirijo
					if (element.hasAttribute ('data-page-title')) {
						window.history.pushState (null, element.getAttribute ('data-page-title'), element.getAttribute (attribute))
						document.title = element.getAttribute ('data-page-title')
					} else {
						window.history.pushState (null, null, element.getAttribute (attribute))
					}
					that.change_state_event_handler (event)
					that._onpushstate ()
				})
				element.setAttribute ('data-TlFwszPG', 'CgfrORUh')
			}
		})
	}

	// set loading_bar (element) {
	// 	this._loading_bar = element
	// }

	// set onpushstate (func) {
	// 	this._onpushstate = func
	// }

	get animations () {
		return this._animations
	}

	get controllers () {
		return this._controllers
	}
}

class FraviController extends multiple ([Initiable, Runnable]) {
	constructor (route, displayed_page_route, displayed_in_element, state_handlers = [], scroll_to_top = false, loading_bar = null, animations = []) {
		super ()
		this._route = route
		this.displayed_page_route = displayed_page_route
		// Object.defineProperty (this, 'displayed_page_route', {
		// 	value: displayed_page_route,
		// 	writable: false,
		// 	enumerable: true,
		// 	configurable: false
		// })
		this.onload_request = new XMLHttpRequest ()
		this.displayed_in_element = displayed_in_element
		this.scroll_to_top = scroll_to_top
		this.state_handlers = state_handlers
		this._loading_bar = loading_bar
		this._initialised = false
		// this.app = null
		this._animations = [...animations]
		Object.seal (this)
		Object.freeze (this)
	}

	get route () {
		return this._route
	}

	get initialised () {
		return this._initialised
	}

	set initialised (value) {
		this._initialised = value
	}

	add_state_handler (handler) {
		this.state_handlers.push (handler)
	}

	set animations (list) {
		this._animations = [...list]
	}

	append_animation (css_class) {
		this._animations.push (css_class)
	}

	extend_animation (css_class) {
		this._animations = [...this._animations, ...css_class]
	}

	set loading_bar (element) {
		this._loading_bar = element
	}

	get has_loading_bar () {
		return this._loading_bar != null
	}

	init (app, post_load = null) {
		// if (this.app == null) {
		// 	Object.defineProperty (this, 'app', {
		// 		value: app,
		// 		writable: true,
		// 		enumerable: true,
		// 		configurable: true
		// 	})
		// }
		// this.app = app

		// console.log (`Init FraviController for: ${this._route}`)
		let that = this
		this.onload_request.addEventListener ('readystatechange', () => {
			if (this.onload_request.readyState == 4) {
				if (that.route == window.location.pathname) {
					this.state_handlers.forEach (state_handler => {
						if (state_handler.code == this.onload_request.status) {
							if ([200, 304].includes (this.onload_request.status)) {
								// quitamos las clases de animación
								// that._animations.forEach (css => {
								// if (that.displayed_in_element.classList.contains (css))
								// 		that.displayed_in_element.classList.remove (css)
								// })
								let container = typeof that.displayed_in_element === 'string' ? document.querySelector (that.displayed_in_element) : that.displayed_in_element
								// console.log (container)
								// console.log (that._route)
								// console.log (that.displayed_page_route)
								// console.log (this.onload_request.responseText)
								container.innerHTML = this.onload_request.responseText
								// that.displayed_in_element.innerHTML = this.onload_request.responseText
								// setTimeout (() => {
								// 	that._animations.forEach (css => {
								// 		if (!(that.displayed_in_element.classList.contains (css)))
								// 			that.displayed_in_element.classList.add (css)
								// 	})
								// }, 10)
								// Añadimos las clases de animación
							}

							// state_handler.run (this.app)
							state_handler.run ()
						}
						
						if (this._loading_bar != null)
							this.end_bar_loading ()

						if (post_load != null)
							post_load ()
						
					})
				}
			}
		})
		this._initialised = true
	}
	
	run (params = null, method = 'GET', form_data = null) {
		if (params !== null) {
			console.log (this.displayed_page_route + '?' + params)
			this.onload_request.open (method, this.displayed_page_route + '?' + params)
		} else {
			console.log (this.displayed_page_route)
			this.onload_request.open (method, this.displayed_page_route)
		}
		if (form_data !== null) {
			this.onload_request.send (form_data)
		} else {
			this.onload_request.send ()
		}
	}

	end_bar_loading () {
		if (this._loading_bar != null) {
			this._loading_bar.getElementsByTagName ('div')[0].style.width = '100%'
			let that = this
			setTimeout (() => {
				if (!that._loading_bar.classList.contains ('d-none'))
					that._loading_bar.classList.add ('d-none')
				
				console.log (`scroll_to_top: ${that.scroll_to_top}`)
				if (that.scroll_to_top) {
					setTimeout (() => {
						window.scrollTo (0, 0)
					}, 300)
				}
			}, 250)
		}
	}
}

class FraviStateHandler extends Runnable {
	constructor (code, handler) {
		super ()
		this._code = code
		this._handler = handler
		Object.seal (this)
		Object.freeze (this)
	}

	get code () {
		return this._code
	}

	get handler () {
		return this._handler
	}

	run (app = null) {
		if (typeof this.handler == 'function')
			try {
				this.handler (app)
				
			} catch (error) {
				this.handler ()
			}

		else if (typeof this.handler == 'object')
			if (Array.isArray (this.handler))
				this.handler.forEach (func => {
					try {
						func (app)
						
					} catch (error) {
						func ()
					}
				})

			else
				Object.entries (this.handler).forEach (([key, func]) => {
					try {
						func (app)
						
					} catch (error) {
						func ()
					}
				})
		else
			console.error ('FraviError: Handler must be functions, array of functions or object with functions as values')
	}
}

class FraviMultipleController extends multiple ([Runnable, Initiable]) {
	constructor (route, subcontrollers) {
		super ()
		this._route = route
		this._subcontrollers = [...subcontrollers]
		this._all_initialised = false
		this.app = null
		Object.seal (this)
		Object.freeze (this)
	}

	get route () {
		return this._route
	}

	get subcontrollers () {
		return this._subcontrollers
	}

	get initialised () {
		return this._all_initialised
	}

	init (app, post_load) {
		this._subcontrollers.forEach (subcontroller => {
			subcontroller.init (app, post_load)
			subcontroller.route = this._route
		})
		let success = this._subcontrollers.map (subcontroller => subcontroller.initialised)
		if (success.every (result => result))
			throw new Error (`FraviMultiController on route '${this._route}' not completly initialized`)

		this._all_initialised = true
	}

	run (params) {
		this._subcontrollers.forEach (subcontroller => {
			subcontroller.run (params)
		})
	}
}

class FraviSubController extends multiple ([Runnable, Initiable]) {
	constructor (displayed_page_route, displayed_in_element, state_handlers = [], scroll_to_top = false, loading_bar = null) {
		super ()
		this._displayed_page_route = displayed_page_route
		this._displayed_in_element = displayed_in_element
		this.state_handlers = state_handlers
		this.scroll_to_top = scroll_to_top
		this.loading_bar = loading_bar
		this._initialised = false
		this._request = new XMLHttpRequest ()
		this._route = '/'
	}

	set route (value) {
		this._route = value
	}

	get initialised () {
		return this._initialised
	}

	init (app, post_load) {
		// if (this.app == null)
		// 	this.app = app

		let that = this
		this._request?.addEventListener?. ('readystatechange', () => {
			if (this._request.readyState == 4) {
				if (that._route == window.location.pathname) {
					this.state_handlers.forEach (state_handler => {
						if (state_handler.code == this._request.status) {
							if ([200, 304].includes (this._request.status)) {
								// quitamos las clases de animación
								// that._animations.forEach (css => {
								// if (that.displayed_in_element.classList.contains (css))
								// 		that.displayed_in_element.classList.remove (css)
								// })
								let container = typeof that._displayed_in_element === 'string' ? document.querySelector (that._displayed_in_element) : that._displayed_in_element
								// console.log (container)
								// console.log (that._route)
								// console.log (that.displayed_page_route)
								// console.log (this._request.responseText)
								container.innerHTML = this._request.responseText
								// that.displayed_in_element.innerHTML = this._request.responseText
								// setTimeout (() => {
								// 	that._animations.forEach (css => {
								// 		if (!(that.displayed_in_element.classList.contains (css)))
								// 			that.displayed_in_element.classList.add (css)
								// 	})
								// }, 10)
								// Añadimos las clases de animación
							}

							state_handler.run ()
							// state_handler.run (this.app)
						}
						
						if (this._loading_bar != null)
							this.end_bar_loading ()

						if (post_load != null)
							post_load ()
						
					})
				}
			}
		})
		this._initialised = true
	}

	run (params = null, method = 'GET', form_data = null) {
		if (params !== null) {
			console.log (this._displayed_page_route + '?' + params)
			this._request?.open (method, this._displayed_page_route + '?' + params)
		} else {
			console.log (this._displayed_page_route)
			this._request?.open (method, this._displayed_page_route)
		}
		if (form_data !== null) {
			this._request?.send (form_data)
		} else {
			this._request?.send ()
		}
	}

	end_bar_loading () {
		if (this._loading_bar != null) {
			this._loading_bar.getElementsByTagName ('div')[0].style.width = '100%'
			let that = this
			setTimeout (() => {
				if (!that._loading_bar.classList.contains ('d-none'))
					that._loading_bar.classList.add ('d-none')
				
				console.log (`scroll_to_top: ${that.scroll_to_top}`)
				if (that.scroll_to_top) {
					setTimeout (() => {
						window.scrollTo (0, 0)
					}, 300)
				}
			}, 250)
		}
	}
}
