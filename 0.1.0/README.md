# FRAVI

 - Front-end
 - React
 - Angular
 - Vue
 - Imitation

## 0.1.-
#### Cambio de paradigma
Debido a que una característica que se creía implementada desde el lanzamiento, realmente en producción no funcionaba, se cambia de nivel de versionado.
El mencionado problema consiste en que se había 'dado por hecho' que al crear 2 `FraviController` con la misma url, en la misma `FraviApp` pero que se renderizarían en diferentes contenedores, se ejecutarían correctamente, pero al probarlo en producción, ésto no funciona.

#### Solución propuesta (1)
Es posible que se pueda diseñar una clase llamada `FraviMultiController`, que sea perfectamente capaz de insertar contenido de diferentes peticiones, en diferentes contentedores, con independencia del número de `FraviStateHandler` por controlador y el número de funciones `handler` asignados a cada `FraviStateHandler`.

### 0.1.1
Aún no está empezada, pero se pretende implementar lo siguiente:
1. Parámetros dependientes: Consiste en asignar una serie de parámetros GET a la url solicitada, de forma oculta al hacer una petición

2. `FraviSingletonController`: como su propio nombre indica, son controladores específicos para incorporar el patrón Singleton, es decir, que sólo se ejecutan una vez en cada aplicación. Serían útiles, en el caso de 137 Apps se usaría para cargar una vez las tarjetas de usuario y de aplicaciones.

3. **Comprobación externa de URL:**
Cuando una URL no se encuentra en la aplicación de Fravi, se hace un GET para comprobar si existe, y si existe se le redirige a ella.

### 0.1.0
1. **Implementación de `FraviMultiController`:**
Se pretende hacer una clase que centralice varias peticiones en un mismo controlador asignado a (Obviamente) una única URL, pero que sin embargo cada petición se puede hacer a páginas diferentes, y se pueden renderizar en contentedores diferentes.
Éste controlador también implementa la característica de los `FraviController` que permite ejecutar varios `window.onload` en cada `FraviStateHandler`.

**Observación:**
Es muy probable, que haya que inventar una clase que represente un _Subcontrolador_ dentro del `FraviMultiController`, que lo que haga sea ahorrarse la propiedad `displayed_page_route`, porque en el caso de éste tipo de implementación, y al estar el _Subcontrolador_ embebido en el objeto de tipo `FraviMultiController`, ya comparte ésta propiedad desde la instancia del objeto que lo contiene ((?) Es posible que haya que aclarar ésto).

### 0.0.1
1. **Aceptación de parámetros GET:**
En ésta nueva versión, la librería es capaz de comparar las URI sin tener en cuenta los parámetros `GET` y si los tiene, se los pasa al controlador backend que tiene que renderizar desde el `FraviController`.

2. **Soporte para múltiples controladores frontend:**
Anteriormente, se ejecutaba una sola función para cada `FraviConroller`, en la versión `0.0.1` se puede añadir 1 controlador de la misma manera que en la versión anterior, además es capaz de ejecutar una serie de controladores, ya sean pasados a través de un `Array` o de un `Object`.

3. **Sobreescritura de barras de carga:**
En la anterior versión, sólo se incorporaba la configuración de una barra de navegación para toda la aplicación, en la versión `0.0.1` se indica una para toda la aplicación, aunque se pueden sobreescribir individualmente por cada controlador.

4. **Paso de información de toda la aplicación a los controladores**
Permite pasar toda la información cargada en la aplicación a los controladores front-end, incluyendo la funcionalidad de cambiar de controlador desde el JavaScript que esté utilizando una `FraviApp`.

### 0.0.0
Creación
