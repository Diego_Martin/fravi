# FRAVI

 - Front-end
 - React
 - Angular
 - Vue
 - Imitation

## 0.0.2
Aún no está empezada, pero se pretende implementar lo siguiente:
1. Parámetros dependientes: Consiste en asignar una serie de parámetros GET a la url solicitada, de forma oculta al hacer una petición

2. `FraviSingletonController`: como su propio nombre indica, son controladores específicos para incorporar el patrón Singleton, es decir, que sólo se ejecutan una vez en cada aplicación. Serían útiles, en el caso de 137 Apps se usaría para cargar una vez las tarjetas de usuario y de aplicaciones.

3. **Comprobación externa de URL:**
Cuando una URL no se encuentra en la aplicación de Fravi, se hace un GET para comprobar si existe, y si existe se le redirige a ella

## 0.0.1
1. **Aceptación de parámetros GET:**
En ésta nueva versión, la librería es capaz de comparar las URI sin tener en cuenta los parámetros `GET` y si los tiene, se los pasa al controlador backend que tiene que renderizar desde el `FraviController`.

2. **Soporte para múltiples controladores frontend:**
Anteriormente, se ejecutaba una sola función para cada `FraviConroller`, en la versión `0.0.1` se puede añadir 1 controlador de la misma manera que en la versión anterior, además es capaz de ejecutar una serie de controladores, ya sean pasados a través de un `Array` o de un `Object`.

3. **Sobreescritura de barras de carga:**
En la anterior versión, sólo se incorporaba la configuración de una barra de navegación para toda la aplicación, en la versión `0.0.1` se indica una para toda la aplicación, aunque se pueden sobreescribir individualmente por cada controlador.

4. **Paso de información de toda la aplicación a los controladores**
Permite pasar toda la información cargada en la aplicación a los controladores front-end, incluyendo la funcionalidad de cambiar de controlador desde el JavaScript que esté utilizando una `FraviApp`.

## 0.0.0
Creación
