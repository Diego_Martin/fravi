"use strict"

class FraviApp {
	constructor(loading_bar = null, onpushstate = null, controllers = [], hidden_parameters = []) {
		this._loading_bar = loading_bar
		this._controllers = controllers
		this._hidden_parameters = hidden_parameters
		this._request = new XMLHttpRequest()
		this._visited = []
		this._onpushstate = onpushstate == null ? () => {} : onpushstate

		let that = this
		window.addEventListener("popstate", (event) => {
			that.change_state_event_handler(event)
		})

		window.addEventListener("pushstate", (event) => {
			that.change_state_event_handler(event)
		})

		if (this._loading_bar != null) {
			this._request.addEventListener("readystatechange", () => {
				switch (this._request.readyState) {
					case 1:
						that._loading_bar.getElementsByTagName("div")[0].style.width = "25%"
						break

					case 2:
						that._loading_bar.getElementsByTagName("div")[0].style.width = "50%"
						break

					case 3:
						that._loading_bar.getElementsByTagName("div")[0].style.width = "75%"
						break

					default:
						break
				}
			})
		}
	}

	init(before_request = null) {
		let that = this
		this._controllers.forEach((controller) => {
			try {
				if (!controller.initialised) {
					controller.init(this._request, this, () => {
						that.link_event_listener("[data-link]", "data-link")
						// that.link_a_event_listener ('a', 'href')
						if (before_request != null) before_request()
					})
					if (controller.loading_bar == null) controller.loading_bar = that._loading_bar
				}
			} catch (error) {
				console.log("Controller not initialised")
			}
		})

		this.change_state_event_handler()
	}

	change_state_event_handler(event = {}) {
		let that = this
		this._loading_bar.getElementsByTagName("div")[0].style.width = "0%"
		if (this._loading_bar.classList.contains("d-none")) this._loading_bar.classList.remove("d-none")

		this._controllers.forEach((controller) => {
			if (controller.route.split("?")[0] == window.location.pathname) {
				// console.log (controller.route)
				// console.log (window.location.pathname)
				// console.log (window.location.href)
				try {
					if (window.location.href.split("?").length == 1) {
						controller.run(this._request)
					} else {
						// Lo que se pretende aquí es configurar un parámetro dependiendo del valor de un input, configurado en la variable that._hidden_parameters
						// DEPRECATED: Esto no solo no se ha terminado la funcionalidad sino que ha dejado de funcionar lo que ya funcionaba, utilizar 0.0.1
						// https://subdomain.domain.com:3000/path?return=/login?user=Joe
						// ['https://subdomain.domain.com:3000/path', 'return=/login', 'user=Joe']
						let parts = window.location.href.split("?")
						parts.shift()

						Object.entries(that._hidden_parameters).forEach(([key, element]) => {
							parts.push(`${key}=${element.value}`)
						})

						controller.run(this._request, parts.join("?"))
					}
				} catch (error) {
					console.log("Controller cannot run")
				}
			}
		})
	}

	add_controller(controller) {
		this._controllers.push(controller)
	}

	execute_controller(path, event = {}) {
		let that = this
		this._controllers.forEach((controller) => {
			if (controller.route.split("?")[0] == path) {
				window.history.pushState(null, null, controller.route)
				that.change_state_event_handler(event)
				that._onpushstate()
			}
		})
	}

	link_event_listener(selector, attribute) {
		let that = this
		document.querySelectorAll(selector).forEach((element) => {
			if (!element.hasAttribute("data-TlFwszPG")) {
				element.addEventListener("mousedown", (event) => {
					// console.log (`Enlace ${element} clicado`)
					// console.log (event.button)
					// console.log (event.type)
					if (element.tagName.toLowerCase() != "a") {
						// Cargo la página a la que me dirijo
						if (event.button == 1) {
							// Click en la rueda
							window.open(element.getAttribute(attribute), "_blank")
						} else if (event.button == 0) {
							// Click izquierdo
							if (element.hasAttribute("data-page-title")) {
								window.history.pushState(null, element.getAttribute("data-page-title"), element.getAttribute(attribute))
								document.title = element.getAttribute("data-page-title")
							} else {
								window.history.pushState(null, null, element.getAttribute(attribute))
							}
							that.change_state_event_handler(event)
							that._onpushstate()
						}
					}
				})
				element.setAttribute("data-TlFwszPG", "CgfrORUh")
			}
		})
	}

	link_a_event_listener(selector, attribute) {
		let that = this
		document.querySelectorAll(selector).forEach((element) => {
			if (!element.hasAttribute("data-TlFwszPG")) {
				element.addEventListener("click", (event) => {
					console.log(`Enlace ${element} clicado`)
					// Cargo la página a la que me dirijo
					if (element.hasAttribute("data-page-title")) {
						window.history.pushState(null, element.getAttribute("data-page-title"), element.getAttribute(attribute))
						document.title = element.getAttribute("data-page-title")
					} else {
						window.history.pushState(null, null, element.getAttribute(attribute))
					}
					that.change_state_event_handler(event)
					that._onpushstate()
				})
				element.setAttribute("data-TlFwszPG", "CgfrORUh")
			}
		})
	}

	set loading_bar(element) {
		this._loading_bar = element
	}

	set onpushstate(func) {
		this._onpushstate = func
	}
}

class FraviController {
	constructor(route, displayed_page_route, displayed_in_element, state_handlers = [], scroll_to_top = false, loading_bar = null) {
		this._route = route
		this.displayed_page_route = displayed_page_route
		this.displayed_in_element = displayed_in_element
		this.scroll_to_top = scroll_to_top
		this.state_handlers = state_handlers
		this._loading_bar = loading_bar
		this.initialised = false
		this.app = null
	}

	get route() {
		return this._route
	}

	get initialised() {
		return this._initialised
	}

	set initialised(value) {
		this._initialised = value
	}

	add_state_handler(handler) {
		this.state_handlers.push(handler)
	}

	set loading_bar(element) {
		this._loading_bar = element
	}

	has_loading_bar() {
		return this._loading_bar != null
	}

	init(onload_request, app, post_load = null) {
		if (this.app == null) this.app = app

		let that = this
		onload_request.addEventListener("readystatechange", () => {
			if (onload_request.readyState == 4) {
				if (that.route == window.location.pathname) {
					this.state_handlers.forEach((state_handler) => {
						if (state_handler.code == onload_request.status) {
							if ([200, 304].includes(onload_request.status)) that.displayed_in_element.innerHTML = onload_request.responseText

							state_handler.run(this.app)
						}

						if (this._loading_bar != null) this.end_bar_loading()

						if (post_load != null) post_load()
					})
				}
			}
		})
		this._initialised = true
	}

	run(onload_request, params = null, method = "GET", form_data = null) {
		if (params !== null) {
			onload_request.open(method, this.displayed_page_route + "?" + params)
		} else onload_request.open(method, this.displayed_page_route)

		if (form_data !== null) onload_request.send(form_data)
		else onload_request.send()
	}

	end_bar_loading() {
		if (this._loading_bar != null) {
			this._loading_bar.getElementsByTagName("div")[0].style.width = "100%"
			let that = this
			setTimeout(() => {
				if (!that._loading_bar.classList.contains("d-none")) that._loading_bar.classList.add("d-none")

				console.log(`scroll_to_top: ${that.scroll_to_top}`)
				if (that.scroll_to_top) {
					setTimeout(() => {
						window.scrollTo(0, 0)
					}, 300)
				}
			}, 250)
		}
	}
}

class FraviStateHandler {
	constructor(code, handler) {
		this._code = code
		this._handler = handler
	}

	get code() {
		return this._code
	}

	get handler() {
		return this._handler
	}

	run(app = null) {
		if (typeof this.handler == "function")
			try {
				this.handler(app)
			} catch (error) {
				this.handler()
			}
		else if (typeof this.handler == "object")
			if (Array.isArray(this.handler))
				this.handler.forEach((func) => {
					try {
						func(app)
					} catch (error) {
						func()
					}
				})
			else
				Object.entries(this.handler).forEach(([key, func]) => {
					try {
						func(app)
					} catch (error) {
						func()
					}
				})
		else console.error("FraviError: Handler must be functions, array of functions or object with functions as values")
	}
}
